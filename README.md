[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Written in Perl](https://img.shields.io/badge/Language-Perl-blue)](https://www.perl.org/)
----
# Install Check

This repository contains a Perl script that checks if a given set of programs are installed and whether they are the latest version.

I've developed the script on Linux (Ubuntu & Debian) and Windows (GIT Bash).


## Linux Installation Notes

`aws`

``` shell
curl -LO https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip
unzip awscli-exe-linux-x86_64.zip
sudo ./aws/install --update
```

`az`

``` shell
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
```

`rg`

``` shell
ripgrep_version=$(grep ripgrep version_db_lnx.csv | cut -d, -f2)
curl -LO https://github.com/BurntSushi/ripgrep/releases/download/${ripgrep_version}/ripgrep_${ripgrep_version}_amd64.deb
sudo dpkg -i ripgrep_${ripgrep_version}_amd64.deb
```

`shellcheck`

``` shell
shellcheck_version=$(grep ShellCheck version_db_lnx.csv | cut -d, -f2)
curl -LO https://github.com/koalaman/shellcheck/releases/download/v${shellcheck_version}/shellcheck-v${shellcheck_version}.linux.x86_64.tar.xz
tar xJvf shellcheck-v${shellcheck_version}.linux.x86_64.tar.xz
cp shellcheck-v${shellcheck_version}/shellcheck ~/bin/
```

`emacs`

Initial build/install:

``` shell
sudo apt-get install build-essential git gnutls-bin gnutls-dev libgif-dev libgtk-3-dev libgtk2.0-dev libjpeg-dev libncurses-dev libncurses5-dev libpng-dev libtiff-dev libx11-dev libxpm-dev texinfo mailutils
mkdir ~/Projects
cd ~/Projects
git clone https://github.com/emacs-mirror/emacs.git # or git pull when updating
./autogen.sh
./configure --prefix=$HOME/opt
make install
```

Updating:

``` shell
sudo apt-get install build-essential git gnutls-bin gnutls-dev libgif-dev libgtk-3-dev libgtk2.0-dev libjpeg-dev libncurses-dev libncurses5-dev libpng-dev libtiff-dev libx11-dev libxpm-dev texinfo mailutils
cd ~/Projects/emacs
git pull
./autogen.sh
./configure --prefix=$HOME/opt
make install
```


## Windows Installation Notes

https://alpha.gnu.org/gnu/emacs/pretest/windows/emacs-30/


## License

This collection is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This collection is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this collection. If not, see
[https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).


## References

* Badges hosted by https://shields.io/
* GPLv3 badge found at https://gist.github.com/lukas-h/2a5d00690736b4c3a7ba
