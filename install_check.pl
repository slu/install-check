#!/usr/bin/env perl

# Check versions of installed software

use v5.30;
use feature qw(signatures);
no warnings qw(experimental::signatures);
use utf8;


use Config;
use English;
use File::Basename qw(dirname);
BEGIN {
    if($OSNAME eq "MSWin32") {
        my $module = "Win32::Console::ANSI";
        my $file = $module;
        $file =~ s[::][/]g;
        $file .= '.pm';
        require $file;
        $module->import;

    }
}
use File::Spec;
use Term::ANSIColor;


$|=1; # flush immediately when print'ing
$ENV{MSYS_NO_PATHCONV}=1; # disable translation of paths to/from POSIX/Windows format

my $path_sep = $Config{path_sep};
my @path = split $path_sep, $ENV{PATH};
my @pathext = split ';', $ENV{PATHEXT};
unshift @pathext, '';
my $unicode = $OSNAME ne "MSWin32";
my $check_mark = $unicode ? "\N{CHECK MARK}" : "@";
my $heavy_check_mark = $unicode ? "\N{HEAVY CHECK MARK}" : "@@";
my $right_arrow = $unicode ? "\N{RIGHTWARDS ARROW}" : ">";
my $heavy_right_arrow = $unicode ? "\N{HEAVY WIDE-HEADED RIGHTWARDS ARROW}" : ">>";
my $dotted_circle = $unicode ? "\N{DOTTED CIRCLE}" : "*";


sub find_executable($command, $path) {
    for my $ext (@pathext) {
        my $full_path = File::Spec->catfile($path, "$command$ext");
        return $full_path if -f $full_path;
    }
    return undef;
}


sub which($command) {
    my $full_path;
    for my $path (@path) {
        $full_path = find_executable($command, $path);
        last if $full_path;
    }
    return $full_path;
}


sub program_info(@fields) {
    return {
        latest => $fields[1],
        url => $fields[2],
        command => $fields[3],
        dir_name => $fields[4],
        check => $fields[5],
        regex => $fields[6]
       };
}


sub load_version_db($filename) {
    state $headers = "Program,Latest version,Download URL,Command name,Directory name,Check method,Version regex";
    my %version_db;
    my $col_count;

    open(my $file, '<', $filename) or die "Could not open '$filename' $!\n";
    while (my $line = <$file>) {
        my @fields = split /[,\r\n]/, $line;

        if (!defined $col_count) {
            die "Expected headers: $headers - got: $line" if join(",", @fields) ne $headers;
            $col_count = scalar @fields;
            next;
        }

        $version_db{$fields[0]} = program_info(@fields);
    }
    return \%version_db;
}


sub wmic_version($command) {
    if ($OSNAME eq "msys" or $OSNAME eq "cygwin") {
        $command = qx(cygpath -w "$command");
        chomp($command);
        $command =~ s/\\/\\\\\\\\/g;
        $command = '\"' . $command . '\"';
    } else {
        $command =~ s/\//\\/g;
        $command =~ s/\\/\\\\/g;
        $command = '"' . $command . '"';
    }
    return `wmic datafile where Name=$command get Version /value`;
}


sub version_info($p) {
    return if !$p->{full_path};
    return qx($p->{full_path} "--version") if $p->{check} eq "--version";
    return wmic_version($p->{full_path}) if $p->{check} eq "wmic";
    warn "Unknown check method: $p->{check}";
}


sub print_version_info($p) {
    my $up_to_date;
    if (defined $p->{version_info}) {
        my $installed_version = $p->{version_info} =~ /$p->{regex}/ ? $1 : "N/A";
        $up_to_date = $installed_version eq $p->{latest};

        say $up_to_date
          ? colored(['green'], "$check_mark up-to-date")
          : colored(['yellow'], "$right_arrow new version available at $p->{url}");

        say "Installed version: $installed_version";
        say "Available version: $p->{latest}";
        say "Install directory: ", dirname($p->{full_path});
    } else {
        say "$dotted_circle not found";
    }
    print "\n";
    return $up_to_date;
}


sub banner() {
    say colored(['cyan'], '_'x57);
    say colored(['magenta'], "  ___           _        _ _  ____ _               _    ");
    say colored(['magenta'], " |_ _|_ __  ___| |_ __ _| | |/ ___| |__   ___  ___| | __");
    say colored(['magenta'], "  | || '_ \\/ __| __/ _` | | | |   | '_ \\ / _ \\/ __| |/ /");
    say colored(['magenta'], "  | || | | \\__ \\ || (_| | | | |___| | | |  __/ (__|   < ");
    say colored(['magenta'], " |___|_| |_|___/\\__\\__,_|_|_|\\____|_| |_|\\___|\\___|_|\\_\\\n");
    say colored(['cyan'], " Version 0.2");
    say colored(['cyan'], '_'x57,"\n",color('reset'));
}


sub main() {
    banner();
    my $my_dir = dirname($0);
    my $version_db_name = sprintf("version_db_%s.csv", $OSNAME eq "MSWin32" ? "win" : "lnx");
    my $filename = File::Spec->catfile($my_dir, $version_db_name);
    my $version_db = load_version_db($filename);
    my $my_opt = File::Spec->catfile("$ENV{HOME}", "opt");
    my $all_up_to_date = 1;
    my $not_found_count = 0;

    foreach my $program (sort keys %{$version_db}) {
        print colored(['bold'], "$program ");
        my $p = $version_db->{$program};
        print "[$p->{command}] ";
        $p->{full_path} = find_executable($p->{command}, File::Spec->catfile($my_opt, $p->{dir_name})) if $p->{dir_name};
        $p->{full_path} = which($p->{command}) if !$p->{full_path};
        if (!$p->{full_path} && ($OSNAME eq "MSWin32" || $OSNAME eq "msys" || $OSNAME eq "cygwin")) {
            $p->{full_path} = find_executable($p->{command}, File::Spec->catfile($ENV{PROGRAMFILES}, $p->{dir_name}));
        }
        $p->{version_info} = version_info($p);
        $not_found_count++ if !defined $p->{version_info};
        $all_up_to_date &= print_version_info($p);
    }

    say colored(['bold'], "Done "), $all_up_to_date
      ? colored("$heavy_check_mark all programs are up-to-date", 'green')
      : colored("$heavy_right_arrow new versions available", 'yellow');
    say "Number of programs in DB: ", scalar keys %{$version_db};
    say "Programs not found count: ", $not_found_count if $not_found_count > 0;
    print "\n";
}

main() unless caller;

1;
